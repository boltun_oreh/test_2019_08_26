<?php


class Autoloader
{
    /**
     * @var string
     */
    private $namespace;

    /**
     * @var int
     */
    private $length;

    /**
     * @var string
     */
    private $dir;

    /**
     * Autoload constructor.
     *
     * @param string $namespace Pass the namespace unescaped.
     * @param string $dir
     */
    public function __construct(string $namespace, string $dir)
    {
        $namespace = rtrim($namespace, '\\') . '\\';
        $this->namespace = $namespace;
        $this->length = strlen($namespace);
        $this->dir = rtrim($dir, '/') . '/';
    }

    /**
     * @param string $search
     * @return void
     */
    public function load(string $search)
    {
        if (strncmp($this->namespace, $search, $this->length) !== 0) {
            return;
        }

        $name = substr($search, $this->length);
        $path = $this->dir . str_replace('\\', '/', $name) . '.php';

        if (is_readable($path)) {
            require $path;
        }
    }
}
