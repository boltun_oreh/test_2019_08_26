<?php

use App\Core\App;
use App\Core\ORM\DbAdapterInterface;

require __DIR__ . '/../Autoloader.php';
spl_autoload_register([new Autoloader('App', __DIR__ . '/../src/'), 'load']);

try {
    $app = new App();
    $serviceContainer = $app->getServiceContainer();

    /** @var DbAdapterInterface $dbAdapter */
    $dbAdapter = $serviceContainer->get('db_adapter');

    $query = "
CREATE TABLE IF NOT EXISTS products
(
    id    INT(11)      NOT NULL AUTO_INCREMENT,
    name  VARCHAR(255) NOT NULL,
    price DECIMAL(8, 2),
    PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS orders
(
    id     INT(11) NOT NULL AUTO_INCREMENT,
    user_id   INT(11) NOT NULL,
    status TINYINT,
    PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS orders_products
(
    order_id   INT(11) NOT NULL,
    product_id INT(11) NOT NULL,
    FOREIGN KEY (order_id) REFERENCES orders (id),
    FOREIGN KEY (product_id) REFERENCES products (id),
    UNIQUE (order_id, product_id)
);";

    $dbAdapter->executeQuery($query);
} catch (Throwable $e) {
    echo $e->getMessage();
}
