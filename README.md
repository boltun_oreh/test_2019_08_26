# Test app
This is a test app implementing RESTfull api

Made without any libraries

Requires only php 7.3 and some php extensions

No tests

### Installation
1. Install php 7.3 + MySql
2. Copy `config/parameters.json.dist` to `config/parameters.json` and fulfill it with db parameters
3. Create database
4. Run `php cli/init_db.php` to create db tables

### Api

There are 4 api endpoints:

- generate some products in db
```
HTTP POST
/api/generate-products
request body:
{"count": 20}
```

- list all products
```
HTTP GET
/api/products
```

- create order
```
HTTP POST
/api/orders
request body:
{"product_ids": [21, 24, 26, 27, 34]}
```

- pay order
```
HTTP PUT
/api/orders/pay
request body:
{"order_id": 10, "order_amount": 157.2}
```

### TODO
1. write tests