<?php


namespace App\Core;


use App\Core\Exception\BadConfigurationException;
use App\Core\Http\Response;
use App\Core\Http\ResponseInterface;
use App\Core\ORM\EntityManager;
use App\Entity\User;
use ReflectionClass;
use ReflectionException;

class Controller
{
    /**
     * @var ServiceContainer
     */
    private $serviceContainer;

    /**
     * @param ServiceContainer $serviceContainer
     */
    public function __construct(ServiceContainer $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
    }

    /**
     * @param string $serviceName
     * @return object
     * @throws BadConfigurationException
     */
    protected function getService(string $serviceName): object
    {
        return $this->serviceContainer->get($serviceName);
    }

    /**
     * @return EntityManager
     * @throws BadConfigurationException
     */
    protected function getEntityManager(): EntityManager
    {
        return $this->getService('entity_manager');
    }

    /**
     * @param array $data
     * @param int $status
     * @return ResponseInterface
     * @throws ReflectionException
     */
    protected function createResponse($data, int $status = Response::HTTP_OK): ResponseInterface
    {
        $data = $this->serialize($data);
        $json = htmlspecialchars(json_encode($data), ENT_QUOTES, 'UTF-8');;

        return new Response($json, $status);
    }

    /**
     * @param mixed $data
     * @return mixed
     * @throws ReflectionException
     */
    private function serialize($data)
    {
        if (is_array($data)) {
            foreach ($data as &$item) {
                if (is_object($item)) {
                    $fields = [];

                    $refClass = new ReflectionClass(get_class($item));

                    if ('App\\Entity' === $refClass->getNamespaceName()) {
                        foreach ($refClass->getProperties() as $refProperty) {
                            $fields[] = $refProperty->name;
                        }

                        $values = [];
                        foreach ($fields as $field) {
                            $getter = 'get' . ucfirst($field);
                            $value = $item->$getter();
                            $values[$field] = $value;
                        }

                        $item = $values;
                    } else {
                        $item = (array)$item;
                    }
                }

                if (is_array($item)) {
                    $item = $this->serialize($item);
                }
            }

            return $data;
        } else {
            return $data;
        }
    }

    /**
     * @return User
     */
    protected function getUser(): User
    {
        $user = new User();
        $user->setId(1)
            ->setName('admin');

        return $user;
    }
}
