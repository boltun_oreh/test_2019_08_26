<?php


namespace App\Core;


use App\Core\Exception\BadConfigurationException;

class ServiceContainer
{
    /**
     * @var object[]
     */
    private $services;

    /**
     * @var array
     */
    private $serviceConfig;

    /**
     * @var array
     */
    private $parameters;

    /**
     * @param array $serviceConfig
     * @param array $parameterConfig
     */
    public function __construct(array $serviceConfig, array $parameterConfig)
    {
        $this->services = [];
        $this->parameters = $parameterConfig;
        $this->serviceConfig = $serviceConfig;
    }

    /**
     * @param string $serviceName
     * @return bool
     */
    public function has(string $serviceName): bool
    {
        return isset($this->serviceConfig[$serviceName]);
    }

    /**
     * @param string $serviceName
     * @return object
     * @throws BadConfigurationException
     */
    public function get(string $serviceName): object
    {
        if (!isset($this->services[$serviceName])) {
            $this->services[$serviceName] = $this->initService($serviceName);
        }

        return $this->services[$serviceName];
    }

    /**
     * @param string $name
     * @return mixed
     * @throws BadConfigurationException
     */
    public function getParameter(string $name)
    {
        if (!$this->hasParameter($name)) {
            throw new BadConfigurationException('Unknown parameter ' . $name);
        }

        return $this->parameters[$name];
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasParameter(string $name): bool
    {
        return isset($this->parameters[$name]);
    }

    /**
     * @param $serviceName
     * @return object
     * @throws BadConfigurationException
     */
    private function initService(string $serviceName): object
    {
        if (!$this->has($serviceName)) {
            throw new BadConfigurationException('Unknown service ' . $serviceName);
        }

        $serviceClassName = $this->serviceConfig[$serviceName]['class_name'];
        $argumentsConfig = $this->serviceConfig[$serviceName]['arguments'];

        $arguments = [];
        foreach ($argumentsConfig as $argumentName) {
            if ($this->has($argumentName)) {
                $arguments[] = $this->get($argumentName);
            } else if ($this->hasParameter($argumentName)) {
                $arguments[] = $this->getParameter($argumentName);
            } else {
                $arguments[] = $argumentName;
            }
        }

        return new $serviceClassName(...$arguments);
    }
}
