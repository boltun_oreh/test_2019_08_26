<?php


namespace App\Core\ORM;


use PDOStatement;

interface DbAdapterInterface
{
    /**
     * @return bool
     */
    public function isConnected() : bool;

    /**
     * @param string $query
     * @param array $params
     * @return PDOStatement
     */
    public function executeQuery(string $query, array $params = []) : PDOStatement;

    /**
     * @param string $query
     * @param array $paramsBag
     * @return mixed
     */
    public function batchExecute(string $query, array $paramsBag);

    /**
     * @return mixed
     */
    public function lastInsertId();
}
