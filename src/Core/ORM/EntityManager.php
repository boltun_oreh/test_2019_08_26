<?php


namespace App\Core\ORM;


use App\Core\Exception\InternalException;
use App\Repository\RepositoryInterface;

class EntityManager
{
    /**
     * @var RepositoryInterface[]
     */
    private $repositories;

    /**
     * @var DbAdapterInterface
     */
    private $dbAdapter;

    /**
     * @param DbAdapterInterface $dbAdapter
     */
    public function __construct(DbAdapterInterface $dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }

    /**
     * @param string $entityClassName
     * @return RepositoryInterface
     * @throws InternalException
     */
    public function getRepository(string $entityClassName): RepositoryInterface
    {
        if (!isset($this->repositories[$entityClassName])) {
            $this->repositories[$entityClassName] = $this->initRepository($entityClassName);
        }

        return $this->repositories[$entityClassName];
    }

    /**
     * @param string $entityClassName
     * @return RepositoryInterface
     * @throws InternalException
     */
    public function initRepository(string $entityClassName): RepositoryInterface
    {
        $classNameParts = explode('\\', $entityClassName);
        $shortClassName = array_pop($classNameParts);
        $repositoryClassName = 'App\\Repository\\' . $shortClassName . 'Repository';

        if (!class_exists($repositoryClassName)) {
            throw new InternalException('Missed class ' . $entityClassName);
        }

        return new $repositoryClassName($this->dbAdapter);
    }
}
