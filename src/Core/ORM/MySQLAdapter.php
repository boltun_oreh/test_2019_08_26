<?php


namespace App\Core\ORM;


use App\Core\Exception\DatabaseException;
use Exception;
use PDO;
use PDOStatement;

class MySQLAdapter implements DbAdapterInterface
{
    /**
     * @var PDO
     */
    private $connection;

    /**
     * @var string
     */
    private $host;

    /**
     * @var int
     */
    private $port;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $database;

    /**
     * @param string $host
     * @param int $port
     * @param string $username
     * @param string $password
     * @param string $database
     */
    public function __construct(string $host, int $port, string $username, string $password, string $database)
    {
        $this->host = $host;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
        $this->database = $database;
    }

    /**
     * @throws DatabaseException
     */
    private function initConnection()
    {
        $dsn = sprintf('mysql:host=%s;port=%s;dbname=%s',
            $this->host,
            $this->port,
            $this->database
        );

        $this->connection = new PDO($dsn, $this->username, $this->password);

        if ($this->connection->errorCode()) {
            throw new DatabaseException($this->connection->errorCode() . ' ' . json_encode($this->connection->errorInfo()));
        }
    }

    /**
     * @return bool
     */
    public function isConnected(): bool
    {
        return null !== $this->connection;
    }

    /**
     * @param string $query
     * @param array $params
     * @return bool|PDOStatement
     * @throws DatabaseException
     */
    public function executeQuery(string $query, array $params = null): PDOStatement
    {
        if (null === $this->connection) {
            $this->connection = $this->getConnection();
        }

        $stmt = $this->connection->prepare($query);
        $stmt->execute($params);

        if ($stmt->errorCode() && '00000' !== $stmt->errorCode()) {
            throw new DatabaseException($stmt->errorCode() . ' ' . json_encode($stmt->errorInfo()));
        }

        return $stmt;
    }

    /**
     * @param string $query
     * @param array $paramsBag
     * @throws DatabaseException
     */
    public function batchExecute(string $query, array $paramsBag)
    {
        if (null === $this->connection) {
            $this->connection = $this->getConnection();
        }

        try {
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connection->beginTransaction();

            $stmt = $this->connection->prepare($query);
            foreach ($paramsBag as $params) {
                $stmt->execute($params);
            }

            $this->connection->commit();
        } catch (Exception $e) {
            $this->connection->rollBack();
            throw new DatabaseException($e->getMessage());
        }
    }

    /**
     * @return mixed
     */
    public function lastInsertId()
    {
        return $this->connection->lastInsertId();
    }

    /**
     * @return PDO
     * @throws DatabaseException
     */
    private function getConnection(): PDO
    {
        if (false === $this->isConnected()) {
            $this->initConnection();
        }

        return $this->connection;
    }
}
