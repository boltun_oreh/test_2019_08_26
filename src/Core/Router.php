<?php


namespace App\Core;


use App\Core\Http\Request;
use App\Core\Http\ResponseInterface;
use App\Core\Exception\InternalException;
use App\Core\Exception\MethodNotAllowedException;
use App\Core\Exception\NotFoundException;

class Router
{
    /**
     * @var ServiceContainer
     */
    private $serviceContainer;

    /**
     * @var array
     */
    private $routes;

    /**
     * @param ServiceContainer $serviceContainer
     * @param array $routes
     */
    public function __construct(ServiceContainer $serviceContainer, array $routes)
    {
        $this->serviceContainer = $serviceContainer;
        $this->routes = $routes;
    }

    /**
     * @param Request $request
     * @return ResponseInterface
     * @throws InternalException
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     */
    public function route(Request $request): ResponseInterface
    {
        $path = $request->getRequestUri();

        if (!array_key_exists($path, $this->routes)) {
            throw new NotFoundException('Page not found');
        }

        $routeConfig = $this->routes[$path];

        if (!class_exists($routeConfig['controller'])
            || !method_exists($routeConfig['controller'], $routeConfig['action'])
        ) {
            throw new InternalException();
        }

        if ($request->getMethod() !== $routeConfig['method']) {
            throw new MethodNotAllowedException('Method not allowed');
        }

        $controller = new $routeConfig['controller']($this->serviceContainer);
        $action = $routeConfig['action'];
        return $controller->$action($request);
    }
}
