<?php


namespace App\Core;


use App\Core\Http\Request;
use App\Core\Http\ResponseInterface;
use App\Core\Exception\InternalException;
use App\Core\Exception\MethodNotAllowedException;
use App\Core\Exception\NotFoundException;
use Exception;

class App
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var ServiceContainer
     */
    private $serviceContainer;

    /**
     * App constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * @param Request $request
     * @return ResponseInterface
     * @throws NotFoundException
     * @throws InternalException
     * @throws MethodNotAllowedException
     */
    public function process(Request $request): ResponseInterface
    {
        $response = $this->router->route($request);
        return $response;
    }

    /**
     * @throws Exception
     */
    private function init()
    {
        $config = $this->getConfig();
        $this->serviceContainer = new ServiceContainer($config['services'], $config['parameters']);
        $this->router = new Router($this->serviceContainer, $config['routes']);
    }

    /**
     * @return ServiceContainer
     */
    public function getServiceContainer(): ServiceContainer
    {
        return $this->serviceContainer;
    }

    /**
     * @return array
     * @throws Exception
     */
    private function getConfig(): array
    {
        if (!file_exists('../config/parameters.json')
            || !file_exists('../config/services.json')
            || !file_exists('../config/routes.json')
        ) {
            throw new Exception('Missed app configuration');
        }

        $parametersJson = file_get_contents('../config/parameters.json', true);
        $servicesJson = file_get_contents('../config/services.json', true);
        $routesJson = file_get_contents('../config/routes.json', true);

        $services = json_decode($servicesJson, true);
        $appServices = [
            'db_adapter' => [
                'class_name' => 'App\Core\ORM\MySQLAdapter',
                'arguments' => [
                    "db_host",
                    "db_port",
                    "db_username",
                    "db_password",
                    "db_database",
                ],
            ],
            'entity_manager' => [
                'class_name' => 'App\Core\ORM\EntityManager',
                'arguments' => [
                    "db_adapter",
                ],
            ],
        ];

        $services = array_merge($services, $appServices);
        $parameters = json_decode($parametersJson, true);
        $routes = json_decode($routesJson, true);

        return [
            'services' => $services,
            'parameters' => $parameters,
            'routes' => $routes,
        ];
    }
}
