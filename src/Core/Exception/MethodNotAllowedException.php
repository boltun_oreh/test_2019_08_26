<?php


namespace App\Core\Exception;


use App\Core\Http\Response;
use Exception;
use Throwable;

class MethodNotAllowedException extends Exception
{
    public function __construct(string $message = "", int $code = Response::HTTP_METHOD_NOT_ALLOWED, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
