<?php


namespace App\Core\Exception;


use App\Core\Http\Response;
use Throwable;

class BadRequestException extends \Exception
{
    public function __construct(string $message = "", int $code = Response::HTTP_BAD_REQUEST, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
