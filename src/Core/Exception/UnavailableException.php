<?php


namespace App\Core\Exception;


use Exception;

class UnavailableException extends Exception
{
}
