<?php


namespace App\Core\Http;


class Response implements ResponseInterface
{
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_FORBIDDEN = 403;
    const HTTP_NOT_FOUND = 404;
    const HTTP_METHOD_NOT_ALLOWED = 405;
    const HTTP_INTERNAL_SERVER_ERROR = 500;

    const CODE_TEXTS = [
        self::HTTP_OK => 'OK',
        self::HTTP_CREATED => 'Created',
        self::HTTP_BAD_REQUEST => 'Bad request',
        self::HTTP_UNAUTHORIZED => 'Unauthorized',
        self::HTTP_FORBIDDEN => 'Forbidden',
        self::HTTP_NOT_FOUND => 'Not found',
        self::HTTP_METHOD_NOT_ALLOWED => 'Method not allowed',
        self::HTTP_INTERNAL_SERVER_ERROR => 'Internal server error',
    ];

    /**
     * @var string
     */
    private $content;

    /**
     * @var int
     */
    private $code;

    /**
     * @param mixed $data
     * @param int $code
     */
    public function __construct($data, int $code = self::HTTP_OK)
    {
        $this->content = json_encode($data);
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return Response
     */
    public function setCode(int $code): Response
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getCodeText(): string
    {
        return self::CODE_TEXTS[$this->code];
    }
}
