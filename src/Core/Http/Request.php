<?php


namespace App\Core\Http;


class Request
{
    /**
     * @var array
     */
    private $query;

    /**
     * @var array
     */
    private $cookies;

    /**
     * @var array
     */
    private $files;

    /**
     * @var array
     */
    private $server;

    /**
     * @var array
     */
    private $headers;

    /**
     * @var string
     */
    private $requestUri;

    /**
     * @var string
     */
    private $method;

    /**
     * @var array
     */
    private $content;

    /**
     * @return Request
     */
    public function init(): Request
    {
        $this->query = $_GET;
        $this->cookies = $_COOKIE;
        $this->files = $_FILES;
        $this->server = $_SERVER;
        $this->headers = [];
        $this->requestUri = $_SERVER['PATH_INFO'];
        $this->method = $_SERVER['REQUEST_METHOD'];

        $content = file_get_contents('php://input');
        if (false !== $content) {
            $this->content = json_decode($content, true);
        }

        return $this;
    }

    /**
     * @param $param
     * @return mixed|null
     */
    public function get(string $param)
    {
        if (array_key_exists($param, $this->query)) {
            return $this->query[$param];
        }

        if (array_key_exists($param, $this->content)) {
            return $this->content[$param];
        }

        return null;
    }

    /**
     * @return string
     */
    public function getRequestUri(): string
    {
        return $this->requestUri;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }
}
