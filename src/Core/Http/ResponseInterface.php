<?php


namespace App\Core\Http;


interface ResponseInterface
{
    /**
     * @return int
     */
    public function getCode(): int;

    /**
     * @return string
     */
    public function getContent(): string;

    /**
     * @return string
     */
    public function getCodeText(): string;
}
