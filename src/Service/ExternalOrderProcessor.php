<?php


namespace App\Service;


use App\Entity\Order;


class ExternalOrderProcessor
{
    /**
     * @param Order $order
     * @return bool
     */
    public function process(Order $order) : bool
    {
        $ch = curl_init('https://ya.ru');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY  , true);
        $html = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if (200 !== $httpcode) {
            return false;

}
        return true;
    }
}
