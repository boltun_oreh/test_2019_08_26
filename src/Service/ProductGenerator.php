<?php

namespace App\Service;


use App\Core\Exception\InternalException;
use App\Core\ORM\EntityManager;
use App\Entity\Product;

class ProductGenerator
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @param int $count
     * @return array
     * @throws InternalException
     */
    public function generateProducts(int $count): array
    {
        $result = [];

        for ($i = 0; $i < $count; $i++) {
            $product = new Product();
            $product->setName($this->randomName())
                ->setPrice($this->randomPrice());

            $this->em->getRepository(Product::class)->insert($product);

            $result[] = $product;
        }

        return $result;
    }

    /**
     * @return string
     */
    private function randomName(): string
    {
        $colors = [
            'red',
            'blue',
            'green',
            'black',
            'white',
            'pink',
            'violet',
        ];
        $fooBar = [
            'foo',
            'bar',
        ];

        return $colors[array_rand($colors)] . " " . $fooBar[array_rand($fooBar)];
    }

    /**
     * @return float
     */
    private function randomPrice(): float
    {
        return mt_rand(1, 1000) / 10;
    }
}
