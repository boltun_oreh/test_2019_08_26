<?php


namespace App\Repository;


use App\Core\ORM\DbAdapterInterface;
use ReflectionClass;
use ReflectionException;

abstract class AbstractRepository implements RepositoryInterface
{
    /**
     * @var string
     */
    protected $tableName;

    /**
     * @var string
     */
    protected $entityClass;

    /**
     * @var DbAdapterInterface
     */
    protected $dbAdapter;

    public function __construct(DbAdapterInterface $dbAdapter)
    {
        $this->entityClass = static::ENTITY_CLASS;
        $this->tableName = static::ENTITY_TABLE;
        $this->dbAdapter = $dbAdapter;
    }

    /**
     * @param int $id
     * @return null|object
     */
    public function find(int $id): ?object
    {
        $query = "SELECT * FROM {$this->tableName} WHERE id = ?";
        $params = [
            $id,
        ];

        $stmt = $this->dbAdapter->executeQuery($query, $params);

        $res = $stmt->fetchObject('App\\Entity\\' . $this->entityClass);
        return $res !== false ? $res : null;
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        $query = "SELECT * FROM {$this->tableName}";

        $stmt = $this->dbAdapter->executeQuery($query);

        $result = [];
        while ($row = $stmt->fetchObject('App\\Entity\\' . $this->entityClass)) {
            $result[] = $row;
        }

        return $result;
    }

    /**
     * @param object $entity
     * @return int
     * @throws ReflectionException
     */
    public function insert(object $entity): int
    {
        $columns = $this->getColumns();
        unset($columns[array_search('id', $columns)]);

        $query = sprintf(
            "INSERT INTO %s (%s) VALUES(%s)",
            $this->getTableName(),
            implode(', ', $columns),
            implode(', ', array_fill(0, count($columns), '?'))
        );

        $params = [];

        foreach ($columns as $column) {
            $getter = 'get' . str_replace('_', '', ucwords($column, '_'));
            $value = $entity->$getter();
            $params[] = $value;
        }

        $this->dbAdapter->executeQuery(
            $query,
            $params
        );

        return $this->dbAdapter->lastInsertId();
    }

    /**
     * @param object $entity
     * @throws ReflectionException
     */
    public function update(object $entity): void
    {
        $columns = $this->getColumns();
        unset($columns[array_search('id', $columns)]);

        $setString = '';
        foreach ($columns as $column) {
            $setString .= $column . ' = ?';
        }
        $query = sprintf(
            "UPDATE %s SET %s WHERE id = ?",
            $this->getTableName(),
            $setString
        );

        $params = [];

        foreach ($columns as $column) {
            $getter = 'get' . str_replace('_', '', ucwords($column, '_'));
            $value = $entity->$getter();
            $params[] = $value;
        }

        $params[] = $entity->getId();

        $this->dbAdapter->executeQuery(
            $query,
            $params
        );
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function getColumns(): array
    {
        $columns = [];

        $refClass = new ReflectionClass('App\\Entity\\' . $this->entityClass);

        foreach ($refClass->getProperties() as $refProperty) {
            $columns[] = $refProperty->name;
        }

        return $columns;
    }
}
