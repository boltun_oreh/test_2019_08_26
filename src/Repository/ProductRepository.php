<?php


namespace App\Repository;


class ProductRepository extends AbstractRepository
{
    const ENTITY_CLASS = 'Product';
    const ENTITY_TABLE = 'products';

    /**
     * @param array $ids
     * @return array
     */
    public function findByIds(array $ids): array
    {
        $bindings = array_fill(0, count($ids), '?');
        $query = sprintf(
            "SELECT * FROM %s WHERE id IN(%s)",
            $this->tableName,
            implode(', ', $bindings)
        );

        $stmt = $this->dbAdapter->executeQuery($query, $ids);
        $result = [];

        while ($row = $stmt->fetchObject('App\\Entity\\' . $this->entityClass)) {
            $result[] = $row;
        }

        return $result;
    }
}
