<?php


namespace App\Repository;


class OrderRepository extends AbstractRepository
{
    const ENTITY_CLASS = 'Order';
    const ENTITY_TABLE = 'orders';

    /**
     * @param int $id
     * @return null|object
     */
    public function find(int $id): ?object
    {
        $query = "SELECT id, user_id, status,
               (SELECT CONCAT(GROUP_CONCAT(products.id SEPARATOR ';'), '||', GROUP_CONCAT(products.name SEPARATOR ';'), '||',
                              GROUP_CONCAT(products.price SEPARATOR ';')) AS product
                FROM products
                         JOIN orders_products op on products.id = op.product_id
                WHERE op.order_id = ?) AS products_string
        FROM orders t
        WHERE t.id = ?";

        $stmt = $this->dbAdapter->executeQuery($query, [$id, $id]);

        $res = $stmt->fetchObject('App\\Entity\\' . $this->entityClass);
        return $res !== false ? $res : null;
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        $query = "SELECT * FROM {$this->tableName} t LEFT JOIN orders_products op ON op.order_id = t.id";

        $stmt = $this->dbAdapter->executeQuery($query);

        $result = [];
        while ($row = $stmt->fetchObject('App\\Entity\\' . $this->entityClass)) {
            $result[] = $row;
        }

        return $result;
    }

    /**
     * @param object $order
     * @return int
     */
    public function insert(object $order): int
    {
        $this->dbAdapter->executeQuery(
            "INSERT INTO {$this->getTableName()} (user_id, status) VALUES(?, ?)",
            [
                $order->getUser()->getId(),
                $order->getStatus(),
            ]
        );
        $id = $this->dbAdapter->lastInsertId();

        $params = [];
        foreach ($order->getProducts() as $product) {
            $params[] = [$id, $product->getId()];
        }
        $this->dbAdapter->batchExecute(
            "INSERT INTO orders_products (order_id, product_id) VALUES(?, ?)",
            $params
        );

        return $id;
    }

    /**
     * @param object $order
     */
    public function update(object $order): void
    {
        $this->dbAdapter->executeQuery(
            "UPDATE {$this->getTableName()} SET user_id = ?, status = ? WHERE id = ?",
            [
                $order->getUser()->getId(),
                $order->getStatus(),
                $order->getId(),
            ]
        );
    }
}
