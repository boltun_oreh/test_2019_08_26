<?php


namespace App\Repository;



interface RepositoryInterface
{
    /**
     * @param int $id
     * @return object|null
     */
    public function find(int $id): ?object;

    /**
     * @return array
     */
    public function findAll(): array;

    /**
     * @return string
     */
    public function getTableName(): string;

    /**
     * @return array
     */
    public function getColumns(): array;

    /**
     * @param object $order
     * @return int
     */
    public function insert(object $order): int;

    /**
     * @param object $order
     */
    public function update(object $order): void;
}
