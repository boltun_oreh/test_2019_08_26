<?php

namespace App\Entity;

class Order
{
    const STATUS_NEW = 0;
    const STATUS_PAYED = 1;

    /**
     * @var int
     */
    private $id;

    /**
     * @var Product[]
     */
    private $products;

    /**
     * @var User
     */
    private $user;

    /**
     * @var int
     */
    private $status = self::STATUS_NEW;

    /**
     * @return float
     */
    public function getSummary(): float
    {
        $result = 0;

        foreach ($this->products as $product) {
            $result += $product->getPrice();
        }

        return $result;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Product[]
     */
    public function getProducts(): ?array
    {
        return $this->products;
    }

    /**
     * @param Product[] $products
     * @return Order
     */
    public function setProducts(array $products): Order
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Order
     */
    public function setStatus(int $status): Order
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Order
     */
    public function setUser(User $user): Order
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function __set($name, $value)
    {
        if ('products_string' === $name) {
            $fieldsValues = explode('||', $value);

            foreach ($fieldsValues as &$fieldValues) {
                $fieldValues = explode(';', $fieldValues);
            }

            foreach ($fieldsValues[0] as $i => $productId) {
                $product = new Product();
                $product->setId($productId)
                    ->setName($fieldsValues[1][$i])
                    ->setPrice($fieldsValues[2][$i]);
                $this->products[] = $product;
            }
        } else if ('user_id' === $name) {
            $this->user = new User();
            $this->user->setId($value);
            $this->user->setName('admin');
        }
    }
}
