<?php

namespace App\Controller;


use App\Core\Controller;
use App\Core\Exception\BadConfigurationException;
use App\Core\Exception\BadRequestException;
use App\Core\Http\Request;
use App\Core\Http\Response;
use App\Core\Http\ResponseInterface;
use ReflectionException;

class AdminController extends Controller
{
    /**
     * @param Request $request
     * @return ResponseInterface
     * @throws BadRequestException
     * @throws BadConfigurationException
     * @throws ReflectionException
     */
    public function generateProductsAction(Request $request): ResponseInterface
    {
        $productsCount = $request->get('count');

        if (null === $productsCount) {
            throw new BadRequestException('Missed mandatory parameter "count"');
        }

        $products = $this->getService('product_generator')->generateProducts($productsCount);

        return $this->createResponse([
            'products' => $products,
        ], Response::HTTP_CREATED);
    }
}
