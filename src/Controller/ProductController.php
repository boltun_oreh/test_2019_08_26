<?php

namespace App\Controller;


use App\Core\Controller;
use App\Core\Http\Response;
use App\Core\Http\ResponseInterface;
use App\Entity\Product;
use Exception;

class ProductController extends Controller
{
    /**
     * @return ResponseInterface
     * @throws Exception
     */
    public function listAction() : ResponseInterface
    {
        $products = $this->getEntityManager()->getRepository(Product::class)->findAll();

        return $this->createResponse([
            'products' => $products,
        ], Response::HTTP_OK);
    }
}
