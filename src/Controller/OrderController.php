<?php

namespace App\Controller;


use App\Core\Controller;
use App\Core\Exception\UnavailableException;
use App\Core\Http\Request;
use App\Core\Http\Response;
use App\Core\Http\ResponseInterface;
use App\Entity\Order;
use App\Entity\Product;
use App\Core\Exception\BadConfigurationException;
use App\Core\Exception\BadRequestException;
use App\Core\Exception\InternalException;
use App\Core\Exception\NotFoundException;
use Exception;
use ReflectionException;

class OrderController extends Controller
{
    /**
     * @param Request $request
     * @return ResponseInterface
     * @throws BadRequestException
     * @throws InternalException
     * @throws ReflectionException
     */
    public function createAction(Request $request): ResponseInterface
    {
        $productIds = $request->get('product_ids');

        if (!is_array($productIds) || empty($productIds)) {
            throw new BadRequestException('Invalid parameter \'product_ids\'');
        }

        $em = $this->getEntityManager();
        $products = $em->getRepository(Product::class)->findByIds($productIds);

        if (count($products) < count($productIds)) {
            $foundProductIds = array_map(function (Product $product) {
                return $product->getId();
            }, $products);

            $missedProductIds = array_diff($productIds, $foundProductIds);

            throw new Exception('Products with ids ' . implode(', ', $missedProductIds) . ' not found');
        }

        $order = new Order();
        $order->setProducts($products)
            ->setUser($this->getUser());
        $orderId = $em->getRepository(Order::class)->insert($order);

        return $this->createResponse([
            'order' => $orderId,
        ], Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @return ResponseInterface
     * @throws BadRequestException
     * @throws InternalException
     * @throws NotFoundException
     * @throws BadConfigurationException
     * @throws ReflectionException
     * @throws UnavailableException
     */
    public function payAction(Request $request): ResponseInterface
    {
        $orderId = $request->get('order_id');
        $orderAmount = (float)$request->get('order_amount');

        if (empty($orderId)) {
            throw new BadRequestException('invalid parameter \'order_id\'');
        }
        if (empty($orderAmount)) {
            throw new BadRequestException('invalid parameter \'order_amount\'');
        }

        $em = $this->getEntityManager();

        /** @var Order $order */
        $order = $em->getRepository(Order::class)->find($orderId);

        if (null === $order) {
            throw new NotFoundException('order with id ' . $orderId . ' not found');
        }

        if ($orderAmount !== $order->getSummary()) {
            throw new BadRequestException('invalid order amount');
        }

        $externalOrderProcessor = $this->getService('external_order_processor');
        $orderProcessResult = $externalOrderProcessor->process($order);

        if (false === $orderProcessResult) {
            throw new UnavailableException('service unavailable');
        }

        $order->setStatus(Order::STATUS_PAYED);
        $em->getRepository(Order::class)->update($order);

        return $this->createResponse([
            'order' => $order,
        ], Response::HTTP_OK);
    }
}
