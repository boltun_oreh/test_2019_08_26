<?php

use App\Core\App;
use App\Core\Http\Request;
use App\Core\Http\Response;

require __DIR__ . '/../Autoloader.php';
spl_autoload_register( [ new Autoloader( 'App', __DIR__ . '/../src/' ), 'load' ] );

try {
    $app = new App();
    $request = new Request();
    $request->init();
    $response = $app->process($request);
} catch (Throwable $e) {
    $response = new Response([
        'error' => $e->getMessage(),
    ]);
    $response->setCode($e->getCode());
}

header(sprintf('HTTP/1.1 %s %s', $response->getCode(), $response->getCodeText()), true, $response->getCode());
echo $response->getContent();
exit();
